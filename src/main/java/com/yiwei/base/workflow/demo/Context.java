package com.yiwei.base.workflow.demo;

public interface Context {
    String getName();
    Object getState();
}
