package com.yiwei.base.workflow.demo;

import com.yiwei.base.workflow.demo.command.Command;
import com.yiwei.base.workflow.demo.engine.Workflow;

public abstract class Node {
    /**
     * 节点key
     */
    protected String key;
    /**
     * 节点处理人
     */
    protected String name;
    /**
     * 后续待处理的节点
     */
    protected Node next;
    /**
     * 执行的工作
     */
    protected Command command;
    /**
     * 是否完成了当前节点的任务
     */
    protected boolean suspend = false;

    /**
     * 关联的工作流
     */
    protected Workflow workflow;

    public Node(String key, String name) {
        this.key = key;
        this.name = name;
        this.command = null;
    }

    public Node(String key, String name, Command command) {
        this.key = key;
        this.name = name;
        this.command = command;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

    public void setWorkflow(Workflow workflow) {
        this.workflow = workflow;
    }

    public void setSuspend(boolean suspend) {
        this.suspend = suspend;
    }

    public abstract void process(Context context);

}
