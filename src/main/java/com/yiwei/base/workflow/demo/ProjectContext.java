package com.yiwei.base.workflow.demo;

public class ProjectContext implements Context{
    /**
     * 任务名称
     */
    private String projectName;
    /**
     * 任务状态
     */
    private State state = State.INIT;

    public ProjectContext(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName(){
        return projectName;
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState(){
        return state;
    }

    @Override
    public String getName() {
        return projectName;
    }
}
