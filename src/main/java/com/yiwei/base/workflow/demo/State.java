package com.yiwei.base.workflow.demo;

public enum State {
    INIT(0, "初始化"),
    PACKAGEING(1, "打包中"),
    PACKAGE_SUCCESS(2, "打包成功"),
    PACKAGE_FAIL(3, "打包失败"),
    CHECKING(4, "发布前的检查"),
    CHECK_SUCCESS(5, "检查成功"),
    CHECK_FAIL(6, "检查失败"),
    PUBLISHING(7, "发布中"),
    PUBLISH_SUCCESS(8, "发布成功"),
    PUBLISH_FAIL(9, "发布失败"),
    PUBLISH_STOP(10, "发布暂停"),
    PUBLISH_CLOSE(11, "发布终止");

    private int id;
    private String name;

    State(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
