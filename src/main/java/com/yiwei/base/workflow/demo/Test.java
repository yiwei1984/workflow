package com.yiwei.base.workflow.demo;

import com.yiwei.base.workflow.demo.command.BuildCommand;
import com.yiwei.base.workflow.demo.command.CheckCommand;
import com.yiwei.base.workflow.demo.command.Command;
import com.yiwei.base.workflow.demo.engine.Workflow;
import com.yiwei.base.workflow.demo.step.Check;
import com.yiwei.base.workflow.demo.step.CheckCross;
import com.yiwei.base.workflow.demo.step.Finish;
import com.yiwei.base.workflow.demo.step.Package;
import com.yiwei.base.workflow.demo.step.PackageCross;
import com.yiwei.base.workflow.demo.step.Publish;
import com.yiwei.base.workflow.demo.tool.JenkinsTool;
import com.yiwei.base.workflow.demo.tool.LinuxTool;

public class Test {
    public static void main(String[] args) throws InterruptedException {
        ProjectContext context = new ProjectContext("crm");

        JenkinsTool jenkinsTool = new JenkinsTool();
        Command build = new BuildCommand(jenkinsTool);
        Package pk = new Package("build","打包", build);
        PackageCross pkCross = new PackageCross("buildCross","打包结果判断");

        LinuxTool linuxTool = new LinuxTool();
        Command check = new CheckCommand(linuxTool);
        Check ck = new Check("check","检查", check);
        CheckCross ckCross = new CheckCross("checkCross","检查结果判断");
        Publish pb = new Publish("publish","发布");
        Finish finish = new Finish("publishClose","流程结束");


//        pk.setNext(pkCross);
//        pkCross.setNext(ck);
//        ck.setNext(ckCross);
//        ckCross.setNext(pb);
//        pb.setNext(finish);
//
//        pk.process(context);
        Workflow workflow = new Workflow("crm-publish","crm系统发布",pk);
        workflow.append(pkCross);
        workflow.append(ck);
        workflow.append(ckCross);
        workflow.append(pb);
        workflow.append(finish);
        workflow.printNodes();
        System.out.println();
        workflow.start(context);
//        Thread.sleep(4000);
//        workflow.stop();
//        workflow.close();
    }
}
