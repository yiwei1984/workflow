package com.yiwei.base.workflow.demo.command;

import com.yiwei.base.workflow.demo.Context;
import com.yiwei.base.workflow.demo.ProjectContext;
import com.yiwei.base.workflow.demo.State;
import com.yiwei.base.workflow.demo.tool.JenkinsTool;

public class BuildCommand implements Command {
    private JenkinsTool jenkinsTool;

    public BuildCommand(JenkinsTool tool) {
        this.jenkinsTool = tool;
    }

    @Override
    public void execute(Context context) {
        ProjectContext ctx = (ProjectContext)context;
        if (jenkinsTool.build(ctx.getProjectName())) {
            ctx.setState(State.PACKAGE_SUCCESS);
        } else {
            ctx.setState(State.PACKAGE_FAIL);
        }
    }
}
