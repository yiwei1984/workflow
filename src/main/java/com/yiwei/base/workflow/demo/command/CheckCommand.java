package com.yiwei.base.workflow.demo.command;

import com.yiwei.base.workflow.demo.Context;
import com.yiwei.base.workflow.demo.ProjectContext;
import com.yiwei.base.workflow.demo.State;
import com.yiwei.base.workflow.demo.tool.LinuxTool;

public class CheckCommand implements Command {
    private LinuxTool tool;
    public CheckCommand(LinuxTool tool){
        this.tool = tool;
    }
    @Override
    public void execute(Context context) {
        ProjectContext ctx = (ProjectContext)context;
        boolean res;
        res = tool.checkPackage(ctx) && tool.checkTomcat(ctx);
        if(res){
            ctx.setState(State.CHECK_SUCCESS);
        }else {
            ctx.setState(State.CHECK_FAIL);
        }
    }
}
