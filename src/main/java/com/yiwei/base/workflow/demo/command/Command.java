package com.yiwei.base.workflow.demo.command;

import com.yiwei.base.workflow.demo.Context;

public interface Command {
    void execute(Context context);
}
