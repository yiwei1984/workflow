package com.yiwei.base.workflow.demo.controller;

import com.yiwei.base.workflow.demo.ProjectContext;
import com.yiwei.base.workflow.demo.engine.Workflow;
import com.yiwei.base.workflow.demo.engine.WorkflowManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Autowired
    WorkflowManager workflowManager;

    @RequestMapping("/")
    String home() {
        return "Hello World!";
    }

    @RequestMapping("/start")
    String start(){
        ProjectContext context = new ProjectContext("crm");
        Workflow workflow = workflowManager.buildPublish("crm");
        workflow.start(context);
        return "start a workflow";
    }

    @RequestMapping("/stop")
    String stop(){
        workflowManager.stop("");
        return "stop a workflow";
    }

    @RequestMapping("/resume")
    String resume(){
        workflowManager.resume("");
        return "resume a workflow";
    }

    @RequestMapping("/close")
    String close(){
        workflowManager.close("");
        return "close a workflow";
    }

}
