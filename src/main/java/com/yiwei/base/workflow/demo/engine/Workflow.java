package com.yiwei.base.workflow.demo.engine;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.yiwei.base.workflow.demo.Context;
import com.yiwei.base.workflow.demo.Node;

import java.util.Map;
import java.util.TreeMap;

public class Workflow {
    private String code;
    private String name;
    private Node first;
    private Node last;
    private Node current;
    private Map<String, Node> nodes = new TreeMap<>();
    private EventBus bus;
    private boolean isStop = false;
    private Context context;

    public Workflow(String code, String name, Node node) {
        this.code = code;
        this.name = name;
        this.first = node;
        this.last = node;
        nodes.put(node.getKey(), node);
        node.setWorkflow(this);
        bus = new EventBus(code);
        bus.register(this);
    }

    public String getCode(){
        return code;
    }

    public void setCurrent(Node node) {
        this.current = node;
    }

    public void append(Node node) {
        last.setNext(node);
        last = node;
        nodes.put(node.getKey(), node);
        node.setWorkflow(this);
    }

    public void start(Context context) {
        Thread t = new Thread(() -> first.process(context));
        t.start();
        this.context = context;
    }

    public void stop() {
        bus.post("stop");
        isStop = true;
    }

    public void resume() {
        if (isStop) {
            bus.post("resume");
        }
    }

    public void close() {
        if (isStop || current == last) {
            bus.post("close");
        }else {
            System.out.println("请先暂停，或者等流程结束");
        }
    }

    @Subscribe
    public void onEvent(Object event) {
        if (event.equals("stop")) {
            System.out.println("流程暂停，当前流程节点：" + current.getName());
            current.getNext().setSuspend(true);
        } else if (event.equals("resume")) {
            current.getNext().setSuspend(false);
            current.getNext().process(context);
        } else if (event.equals("close")) {
            current.getNext().setSuspend(false);
            current.setNext(last);
            current.getNext().process(context);
        }
    }

    public void printNodes() {
        System.out.println("====流程：" + code + "=======");
        nodes.forEach((k, v) -> {
            System.out.println("节点id：" + k + ", 名称：" + v.getName());
        });
    }

    public Node getNode(String id) {
        return nodes.get(id);
    }
}
