package com.yiwei.base.workflow.demo.engine;

import com.yiwei.base.workflow.demo.command.BuildCommand;
import com.yiwei.base.workflow.demo.command.CheckCommand;
import com.yiwei.base.workflow.demo.command.Command;
import com.yiwei.base.workflow.demo.step.Check;
import com.yiwei.base.workflow.demo.step.CheckCross;
import com.yiwei.base.workflow.demo.step.Finish;
import com.yiwei.base.workflow.demo.step.Package;
import com.yiwei.base.workflow.demo.step.PackageCross;
import com.yiwei.base.workflow.demo.step.Publish;
import com.yiwei.base.workflow.demo.tool.JenkinsTool;
import com.yiwei.base.workflow.demo.tool.LinuxTool;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class WorkflowManager {
    private Map<String,Workflow> workflowMap = new ConcurrentHashMap<>();
    public Workflow buildPublish(String name){
        JenkinsTool jenkinsTool = new JenkinsTool();
        Command build = new BuildCommand(jenkinsTool);
        Package pk = new Package("build","打包", build);
        PackageCross pkCross = new PackageCross("buildCross","打包结果判断");
        LinuxTool linuxTool = new LinuxTool();
        Command check = new CheckCommand(linuxTool);
        Check ck = new Check("check","检查", check);
        CheckCross ckCross = new CheckCross("checkCross","检查结果判断");
        Publish pb = new Publish("publish","发布");
        Finish finish = new Finish("publishClose","流程结束");
        Workflow workflow = new Workflow("crm-1",name,pk);
        workflow.append(pkCross);
        workflow.append(ck);
        workflow.append(ckCross);
        workflow.append(pb);
        workflow.append(finish);
        workflow.printNodes();
        workflowMap.put(workflow.getCode(),workflow);
        return workflow;
    }

    public void stop(String code){
        Workflow workflow = workflowMap.get("crm-1");
        workflow.stop();
    }

    public void resume(String code){
        Workflow workflow = workflowMap.get("crm-1");
        workflow.resume();
    }

    public void close(String code){
        Workflow workflow = workflowMap.get("crm-1");
        workflow.close();
        workflowMap.remove(code);
    }
}
