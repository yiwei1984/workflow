package com.yiwei.base.workflow.demo.step;

import com.yiwei.base.workflow.demo.Context;
import com.yiwei.base.workflow.demo.Node;
import com.yiwei.base.workflow.demo.ProjectContext;
import com.yiwei.base.workflow.demo.State;
import com.yiwei.base.workflow.demo.command.Command;

public class Check extends Node {

    public Check(String key, String name, Command command) {
        super(key, name, command);
    }

    public void process(Context context) {
        ProjectContext ctx = (ProjectContext)context;
        if (context.getState().equals(State.PACKAGE_SUCCESS)) {
            workflow.setCurrent(this);
            ctx.setState(State.CHECKING);
            System.out.println("项目：" + ctx.getProjectName() + " 发布前的检查...");
            command.execute(ctx);
            System.out.println("项目：" + ctx.getProjectName() + " 发布前的检查结果：" + ctx.getState().getName());
            if (!suspend)
                next.process(context);
        }
    }
}
