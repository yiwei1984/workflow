package com.yiwei.base.workflow.demo.step;

import com.yiwei.base.workflow.demo.Context;
import com.yiwei.base.workflow.demo.Node;
import com.yiwei.base.workflow.demo.ProjectContext;
import com.yiwei.base.workflow.demo.State;

public class CheckCross extends Node {
    public CheckCross(String key,String name) {
        super(key ,name);
    }

    @Override
    public void process(Context context) {
        ProjectContext ctx = (ProjectContext)context;
        if(context.getState().equals(State.CHECK_SUCCESS)){
            if(!suspend)
                next.process(context);
        }else {
            System.out.println("项目：" + ctx.getProjectName() + " 流程结束，结果：" + ctx.getState().getName());
        }
    }
}
