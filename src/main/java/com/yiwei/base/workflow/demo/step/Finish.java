package com.yiwei.base.workflow.demo.step;

import com.yiwei.base.workflow.demo.Context;
import com.yiwei.base.workflow.demo.Node;
import com.yiwei.base.workflow.demo.ProjectContext;

public class Finish extends Node {
    public Finish(String key, String name) {
        super(key, name);
    }

    public void process(Context context) {
        ProjectContext ctx = (ProjectContext)context;
        System.out.println("项目：" + ctx.getProjectName() + " 流程执行结束，执行结果：" + ctx.getState().getName());
    }
}
