package com.yiwei.base.workflow.demo.step;

import com.yiwei.base.workflow.demo.Context;
import com.yiwei.base.workflow.demo.Node;
import com.yiwei.base.workflow.demo.ProjectContext;
import com.yiwei.base.workflow.demo.State;
import com.yiwei.base.workflow.demo.command.Command;

public class Package extends Node {

    public Package(String key, String name, Command command) {
        super(key,name,command);
    }

    @Override
    public void process(Context context) {
        ProjectContext ctx = (ProjectContext)context;
        if (context.getState().equals(State.INIT)) {
            workflow.setCurrent(this);
            ctx.setState(State.PACKAGEING);
            System.out.println("项目：" + ctx.getProjectName() + " 打包中...");
            command.execute(ctx);
            System.out.println("项目：" + ctx.getProjectName() + " 打包结果：" + ctx.getState().getName());
            if(!suspend)
                next.process(context);
        }
    }
}
