package com.yiwei.base.workflow.demo.step;

import com.yiwei.base.workflow.demo.Context;
import com.yiwei.base.workflow.demo.Node;
import com.yiwei.base.workflow.demo.ProjectContext;
import com.yiwei.base.workflow.demo.State;

public class Publish extends Node {
    public Publish(String key, String name) {
        super(key, name);
    }

    public void process(Context context) {
        ProjectContext ctx = (ProjectContext)context;
        if (context.getState().equals(State.CHECK_SUCCESS)) {
            workflow.setCurrent(this);
            ctx.setState(State.PUBLISHING);
            System.out.println("项目：" + ctx.getProjectName() + " 发布中...");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ctx.setState(State.PUBLISH_SUCCESS);
            next.process(context);
        }
    }
}
