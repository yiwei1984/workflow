package com.yiwei.base.workflow.demo.tool;

import com.yiwei.base.workflow.demo.ProjectContext;

public class LinuxTool {
    public boolean checkPackage(ProjectContext context){
        System.out.println("服务器开始检查包" + context.getProjectName() + ".war 是否存在...");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean checkTomcat(ProjectContext context){
        System.out.println("服务器开始检查tomcat是否存在...");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }
}
